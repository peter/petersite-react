import './App.css';
import React from "react";


function Search(props) {
  const {searchTerm, handleInputChange, children} = props;
  return (
      <form>
        {children} <input type="text" placeholder="Pesquisar pelo nome do artigo"
          name="searchTerm" value={searchTerm} 
          onChange={(event) => handleInputChange(event)}/>
      </form>
    );
}

function Button(props) {
  const {
    onClick,
    className='',
    children,
  } = props;

  return (
    <button
      onClick={onClick}
      className={className}
      type="button"
    >
      {children}
    </button>
  );
}

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { list: null, searchTerm: '' };
  }

  onDismiss(id) {
    const updatedList = this.state.list.filter(item => item.id !== id);
    this.setState({ list: updatedList });
  }

  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value });
}

  handleInputChange(event) {
    const target = event.target;
    const value = 
      target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  componentDidMount() {
    fetch('http://127.0.0.1:8000/api/v1/enciclopedia/')
      .then((response) => response.json())
      .then((result) => this.setState({ list: result }))
      .catch((error) => error);
  }

  render() {
    const {list, searchTerm} = this.state;
    
    return (
      <div className="App">
        <Search
          searchTerm={searchTerm}
          handleInputChange={(e) => this.handleInputChange(e)}
        >
          Search term:
        </Search>
        { list && (
          <EnciclopediaTable 
            list={list} 
            searchTerm={searchTerm}
            onDismiss={(id) => this.onDismiss(id)}
          />
        )}
      </div>
    );
  }
}



function EnciclopediaTable(props) {
  const { list, onDismiss, searchTerm} = props;
  return (
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Título</th>
            <th>Assunto</th>
            <th>Conteúdo</th>
            <th>Id da categoria</th>
          </tr>
        </thead>
        <tbody>
          {list.filter(enciclopedia => enciclopedia.name.toLowerCase().includes(searchTerm.toLowerCase()))
          .map((enciclopedia) => (
            <tr key={enciclopedia.id}>
              <td>{enciclopedia.id}</td>
              <td>{enciclopedia.name}</td>
              <td>{enciclopedia.assunto}</td>
              <td>{enciclopedia.conteudo}</td>
              <td>{enciclopedia.tipocategoria}</td>
              <td>
              <Button onClick ={() => onDismiss(enciclopedia.id)}>
                Dismiss
              </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
  );
}



export default App;
